
///===========================================================
///===========================================================
//		For Zombie Panic! Source [3.1] and higher
//		Written by Johan "JonnyBoy0719" Ehrendahl
///===========================================================
///===========================================================

// Our boolean array
bool bVoteCreated;
bool bVoteOngoing;
bool bVoteCountDown;

float flRTVTimer;
float flTryStartVote;
float g_flRTVTimer;
float flWaitTimer;

int m_iCountDown;

array<bool> arrPlayerRTV;
array<string> arrNominatedMap;
array<string> g_NominationList;

JsonValues@ hJsonData = null;


void OnPluginInit()
{
	SetPluginMeta();
	LoadOrCreateConfig();
	Translate.AddTranslation( "rockthevote" );
	RegisterHooks();
	PluginReset();
}

void SetPluginMeta()
{
	PluginData::SetVersion( "2.0.0.0" );
	PluginData::SetAuthor( "Chris \"Majorpayne327\"; Johan \"JonnyBoy0719\" Ehrendahl" );
	PluginData::SetName( "Rock The Vote" );
}

/**
Load the config file or create a default if the file does not exist
**/
void LoadOrCreateConfig()
{
	@hJsonData = FileSystem::Json::ReadFile( "rockthevote" );
	if ( hJsonData is null )
	{
		@hJsonData = FileSystem::Json::CreateJson();
		
		FileSystem::Json::Write( hJsonData, "config", "language", "english" );
		FileSystem::Json::Write( hJsonData, "config", "player_percentage", 80.0f );
		FileSystem::Json::Write( hJsonData, "config", "countdown", 15 );
		FileSystem::Json::Write( hJsonData, "config", "waittimer", 30.0f );
		FileSystem::Json::Write( hJsonData, "config", "mode", 1 );
		
		FileSystem::Json::CreateFile( "rockthevote", hJsonData );
	}
}

void RegisterHooks()
{
	Events::Rounds::CurrentRound.Hook( @RTV_RoundStart );
	
	Events::Player::PlayerSay.Hook( @RTV_PlayerSay );
    
	Events::Player::OnPlayerConnected.Hook( @RTV_OnPlayerConnected );
	Events::Player::OnPlayerDisconnected.Hook( @RTV_OnPlayerDisonnected );
	
	Events::VoteSystem::OnVoteEnd.Hook( @RTV_OnVoteEnd );
}

/**
Hook for starting Round Start
**/
HookReturnCode RTV_RoundStart( int &in iRound )
{
	RTVPrint( Translate.GrabTranslation( GetLanguage(), "RTV_Help" ) );
	return HOOK_HANDLED;
}

string GetLanguage()
{
	return FileSystem::Json::GrabString( hJsonData, "config", "language" );
}

/**
Hook for when the Player says something in the text chat
**/
HookReturnCode RTV_PlayerSay( CZP_Player@ pPlayer, CASCommand@ pArgs )
{
	if ( pArgs is null )
		return HOOK_CONTINUE;
	
	string arg1 = pArgs.Arg( 1 );
	
	if ( IsRTVCommand( arg1, "rtv" ) || IsRTVCommand( arg1, "rockthevote" ) )
	{
		if ( HasVoted( pPlayer ) )
		{
			RTVPrint_Player( pPlayer, Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_AlreadyVoted", UTIL_IntToString( GetVotes() ), UTIL_IntToString( RequiredPlayers() ) ) );
			return HOOK_HANDLED;
		}
		if ( CanVote() )
		{
			// We voted
			SetVote( pPlayer, true );
			
			// Tell everyone that we want to RTV
			RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_WantsToVote", pPlayer.GetPlayerName(), UTIL_IntToString( GetVotes() ), UTIL_IntToString( RequiredPlayers() ) ) );
			
			// Reset it
			flRTVTimer = Globals.GetCurrentTime() + 60;
		}
		else
		{
			if ( flWaitTimer > Globals.GetCurrentTime() )
				RoundJustBegan( pPlayer );
			else
				RTVPrint_Player( pPlayer, "Rock the Vote is already ongoing!" );
		}
		return HOOK_HANDLED;
	}
	else if ( IsRTVCommand( arg1, "nominatelist" ) )
	{
		ShowNominateList( pPlayer );
		RTVPrint_Player( pPlayer, Translate.GrabTranslation( GetLanguage(), "RTV_Help_NominateList" ) );
		return HOOK_HANDLED;
	}
	else if ( IsRTVCommand( arg1, "nominate", true ) )
	{
		if ( CanVote() )
		{
			CASCommand@ pSplitArgs = StringToArgSplit( arg1, " " );
			if ( pSplitArgs.Args() < 2 )
			{
				RTVPrint_Player( pPlayer, Translate.GrabTranslation( GetLanguage(), "RTV_Error_Nominate" ) );
				return HOOK_HANDLED;
			}
			
			string arg2 = pSplitArgs.Arg( 1 );
			if ( IsMapValid( arg2 ) )
			{
				if ( IsMapNominated( arg2 ) )
				{
					RTVPrint_Player( pPlayer, Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Error_Nominate_Map", arg2 ) );
					return HOOK_HANDLED;
				}
				
				if ( Utils.StrEql( Globals.GetCurrentMapName(), arg2 ) )
				{
					RTVPrint_Player( pPlayer, Translate.GrabTranslation( GetLanguage(), "RTV_Error_Nominate_Map_Current" ) );
					return HOOK_HANDLED;
				}
				
				CBasePlayer@ pPlayerEnt = pPlayer.opCast();		// Convert to CBasePlayer
				CBaseEntity@ pBaseEnt = pPlayerEnt.opCast();	// Convert to CBaseEntity
				
				if ( Utils.StrEql( arrNominatedMap[ pBaseEnt.entindex() ], "", true ) )
					RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_Nominated", pPlayer.GetPlayerName(), arg2 ) );
				else
					RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_NominatedChanged", pPlayer.GetPlayerName(), arrNominatedMap[ pBaseEnt.entindex() ], arg2 ) );
				
				// Add to nomination list
				AddToNominationList( arrNominatedMap[ pBaseEnt.entindex() ], arg2 );
				
				// Nominate for the map
				arrNominatedMap[ pBaseEnt.entindex() ] = arg2;
			}
			else
				RTVPrint_Player( pPlayer, Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_InvalidMap", arg2 ) );
			
			return HOOK_HANDLED;
		}
		else
		{
			if ( flWaitTimer > Globals.GetCurrentTime() )
				RoundJustBegan( pPlayer );
			else
				RTVPrint_Player( pPlayer, Translate.GrabTranslation( GetLanguage(), "RTV_Error_OnGoing" ) );
		}
	}
	
	return HOOK_CONTINUE;
}

/**
Check to see if the text is a valid RTV command
**/
bool IsRTVCommand( string arg, string value, bool contains = false )
{
	if ( contains )
		return Utils.StrContains( value, arg );
	return Utils.StrEql( arg, value ) || Utils.StrEql( arg, "!" + value ) || Utils.StrEql( arg, "/" + value );
}

/**
Check to see if the player has already voted
**/
bool HasVoted( CZP_Player@ pPlayer )
{
	// Reset the player vote value when they join/leave
	CBasePlayer@ pPlayerEnt = pPlayer.opCast();		// Convert to CBasePlayer
	CBaseEntity@ pBaseEnt = pPlayerEnt.opCast();	// Convert to CBaseEntity
	return arrPlayerRTV[ pBaseEnt.entindex() ];
}

/**
Convert the int into a string
**/
string UTIL_IntToString( int value )
{
	return formatInt( value, "", 2 );
}

/**
Check to see if a player can vote
**/
bool CanVote()
{
	if ( bVoteCountDown ) return false;
	if ( flWaitTimer > Globals.GetCurrentTime() ) return false;
	return true;
}

/**
Set the players vote state
**/
void SetVote( CZP_Player@ pPlayer, bool state )
{
	// Reset the player vote value when they join/leave
	CBasePlayer@ pPlayerEnt = pPlayer.opCast();		// Convert to CBasePlayer
	CBaseEntity@ pBaseEnt = pPlayerEnt.opCast();	// Convert to CBaseEntity
	arrPlayerRTV[ pBaseEnt.entindex() ] = state;
}

/**
Check to see if the round just began
**/
void RoundJustBegan( CZP_Player@ pPlayer )
{
	float waittime = flWaitTimer - Globals.GetCurrentTime();
	int secs = Utils.FloatToInt(waittime % 60.0);
	int formated = Utils.StringToInt( formatInt( secs, "0", 2 ) );
	string strFormat = "second";
	if ( formated > 1 )
		strFormat += "s";
	RTVPrint_Player( pPlayer, Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_MapJustBegan", UTIL_IntToString( secs ), strFormat ) );
}

/**
Display the nominate list to the player
**/
void ShowNominateList( CZP_Player@ pPlayer )
{
	CBasePlayer@ pPlayerEnt = pPlayer.opCast();		// Convert to CBasePlayer
	
	// Grab the mapcycle
	array<string> MapCycle = Globals.GetMapCycle();
	
	// Remove the current map from the list, we aren't allowed to nominate for the current playing map, ye' know
	ExcludeFromVoting( MapCycle, Globals.GetCurrentMapName() );
	
	Chat.PrintToConsolePlayer( pPlayerEnt, Translate.GrabTranslation( GetLanguage(), "RTV_NominateList_Header" ) );
	
	int searchIndex;
	for ( uint i = 0; i < MapCycle.length(); i++ )
	{
		searchIndex = g_NominationList.find( MapCycle[ i ] );
		string strMsg = "{forestgreen}" + MapCycle[ i ];
		
		// If found, its not valid for voting. Display the text as red
		// If not found, its valid. Display the text as green
		if ( searchIndex >= 0 )
			strMsg += " {default}| " + Translate.GrabTranslation( GetLanguage(), "RTV_NominateList_Choosen" );
		
		Chat.PrintToConsolePlayer( pPlayerEnt, strMsg );
	}
	
	Chat.PrintToConsolePlayer( pPlayerEnt, Translate.GrabTranslation( GetLanguage(), "RTV_NominateList_Footer" ) );
}

/**
Print the given message to the given player's chat box
**/
void RTVPrint_Player( CZP_Player@ pPlayer, const string& in strMsg )
{
	CBasePlayer@ pPlayerEnt = pPlayer.opCast();		// Convert to CBasePlayer
	Chat.PrintToChatPlayer( pPlayerEnt, Translate.GrabTranslation( GetLanguage(), "RTV_Tag" ) + " " + strMsg );
}

/**
Check to see if the given map is a valid choice
**/
bool IsMapValid( const string&in strMap )
{
	int searchIndex;
	array<string> MapCycle = Globals.GetMapCycle();
	
	searchIndex = MapCycle.find( strMap );
	
	if ( searchIndex >= 0 )
		return true;
	
	return false;
}

/**
Check to see if the map is nominated
**/
bool IsMapNominated( const string&in strMap )
{
	int searchIndex;
	searchIndex = g_NominationList.find( strMap );
	
	if ( searchIndex >= 0 )
		return true;
	
	return false;
}

/**
Print the given message to all players
**/
void RTVPrint( const string& in strMsg )
{
	Chat.PrintToChat( all, Translate.GrabTranslation( GetLanguage(), "RTV_Tag" ) + " " + strMsg );
}

/**
Add the map to the Nomination List
**/
void AddToNominationList( const string&in strMapPrevious, const string&in strMap )
{
	int searchIndex;
	searchIndex = g_NominationList.find( strMapPrevious );
	
	// If it exist delete it
	if ( searchIndex >= 0 )
		g_NominationList.removeAt( searchIndex );
	
	// Add the new map in
	g_NominationList.insertLast( strMap );
}

/**
Hook for when the Player connects to the server
**/
HookReturnCode RTV_OnPlayerConnected( CZP_Player@ pPlayer )
{
	if ( pPlayer is null ) return HOOK_CONTINUE;
	ResetPlayer( pPlayer );
	return HOOK_CONTINUE;
}

/**
Reset the Players information
**/
void ResetPlayer( CZP_Player@ pPlayer )
{
	// Reset the player vote value when they join/leave
	CBasePlayer@ pPlayerEnt = pPlayer.opCast();		// Convert to CBasePlayer
	CBaseEntity@ pBaseEnt = pPlayerEnt.opCast();	// Convert to CBaseEntity
	arrPlayerRTV[ pBaseEnt.entindex() ] = false;
	arrNominatedMap[ pBaseEnt.entindex() ] = "";
}

/**
Hook for when the player disconnects from the server
**/
HookReturnCode RTV_OnPlayerDisonnected( CZP_Player@ pPlayer )
{
	if ( pPlayer is null ) return HOOK_CONTINUE;
	ResetPlayer( pPlayer );
	return HOOK_CONTINUE;
}

/**
Hook for when a vote ends
**/
HookReturnCode RTV_OnVoteEnd( const string &in strVoteType, const string &in strVoteArg, const string &in strVoteResult, string& out strVoteEndMsg, const int &in iResult )
{
	if ( Utils.StrEql( strVoteArg, "rockthevote" ) )
	{
		if ( !ChangeMapOnVoteEnd() )
		{
			strVoteEndMsg = "The next map will be:\n" + strVoteResult;
			Engine.RunConsoleCommand( "nextlevel " + strVoteResult );
			RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_Vote_NextMap", strVoteResult ) );
		}
		else
		{
			strVoteEndMsg = "Changing level:\n" + strVoteResult;
			RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_Vote_ChangeMap", strVoteResult ) );
			RoundManager.ChangeLevel( strVoteResult );
		}
		return HOOK_HANDLED;
	}
	return HOOK_CONTINUE;
}

/**
Return whether the map should change when the vote ends
**/
bool ChangeMapOnVoteEnd()
{
	return FileSystem::Json::GrabBool( hJsonData, "config", "mode" );
}

void OnProcessRound()
{
	if ( bVoteCreated )
		return;
	
	if ( Globals.GetCurrentTime() > flRTVTimer && bVoteOngoing )
	{
		// Show the msg
		RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_VoteInProgress", UTIL_IntToString( GetVotes() ), UTIL_IntToString( RequiredPlayers() ) ) );
		// Wait 60 seconds until we show the message again
		flRTVTimer = Globals.GetCurrentTime() + 60;
	}
	
	// If there are votes more than 0
	if ( GetVotes() > 0 )
	{
		if ( GetVotes() >= RequiredPlayers()
			&& bVoteCountDown
			&& Globals.GetCurrentTime() > g_flRTVTimer
			&& flTryStartVote == -1 )
		{
			// Begin vote count down
			m_iCountDown--;
			
			if ( m_iCountDown > 0 && m_iCountDown <= 5 )
				RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_VoteBeginsIn", UTIL_IntToString( m_iCountDown ) ) );
			else if ( m_iCountDown == 0 )
				flTryStartVote = Globals.GetCurrentTime();
			
			// Wait each second
			g_flRTVTimer = Globals.GetCurrentTime() + 1.0f;
		}
		
		if ( GetVotes() >= RequiredPlayers() && !bVoteCountDown )
		{
			bVoteCountDown = true;
			g_flRTVTimer = Globals.GetCurrentTime() + 1.0f;
			m_iCountDown = FileSystem::Json::GrabInt( hJsonData, "config", "countdown" );
			
			string strSeconds = m_iCountDown == 1 ? "second" : "seconds";
			RTVPrint( Translate.GrabTranslationFormat_Lang( GetLanguage(), "RTV_Formated_VoteInit", UTIL_IntToString( m_iCountDown ), strSeconds ) );
		}
		
		bVoteOngoing = true;
	}
	else
	{
		flTryStartVote = -1;
		bVoteCountDown = false;
		bVoteOngoing = false;
	}
	
	if ( flTryStartVote != -1 && Globals.GetCurrentTime() > flTryStartVote )
	{
		// Vote ongoing? is so, delay by 5 seconds
		if ( VoteSystem::HasActiveVote() )
			flTryStartVote = Globals.GetCurrentTime() + 5;
		else
		{
			// Reset & stop
			flTryStartVote = -1;
			CreateRTV();
		}
	}
}

/**
Calculate the number of votes casted
**/
int GetVotes()
{
	int iVotes = 0;
	for ( int x = 1; x <= Globals.GetMaxClients(); x++ )
	{
		// Grab our player
		CZP_Player@ pPlayer = ToZPPlayer( x );
		// Make sure they exist
		if ( pPlayer is null ) continue;
		// Did they vote?
		if ( arrPlayerRTV[ x ] )
			iVotes++;
	}
	
	return iVotes;
}

/**
Calculate the number of required votes
**/
int RequiredPlayers()
{
	float flOutPut = 0;
	
	for ( int x = 1; x <= Globals.GetMaxClients(); x++ )
	{
		CZP_Player@ pPlayer = ToZPPlayer( x );
		if ( pPlayer is null ) continue;
		// Valid player, so increase the counter
		flOutPut++;
	}
	
	float flRequired = FileSystem::Json::GrabFloat( hJsonData, "config", "player_percentage" );
	
	// Clamp it
	if ( flRequired < 0.0f )
		flRequired = 0.0f;
	else if ( flRequired > 100.0f )
		flRequired = 100.0f;
	
	flOutPut *= flRequired / 100;
	
	int iResult = int( flOutPut );
	
	if ( iResult <= 0 )
		iResult = 0;
	
	return iResult;
}

/**
Create the RTV vote
**/
void CreateRTV()
{
	CVoteObject@ pVote = VoteSystem::CreateVoteObject();
	pVote.SetType( "generic_radio" );
	pVote.SetValue( "rockthevote" );
	
	// Grab the mapcycle
	array<string> MapCycle = Globals.GetMapCycle();
	
	// Remove the current map from voting, so we can't vote for it twice etc...
	ExcludeFromVoting( MapCycle, Globals.GetCurrentMapName() );
	
	// Set our maps
	pVote.SetArgument( 0, GetAvailableMapVote( MapCycle ) );
	pVote.SetArgument( 1, GetAvailableMapVote( MapCycle ) );
	pVote.SetArgument( 2, GetAvailableMapVote( MapCycle ) );
	pVote.SetArgument( 3, GetAvailableMapVote( MapCycle ) );
	pVote.SetArgument( 4, GetAvailableMapVote( MapCycle ) );
	
	// Our values
	pVote.SetTitle( "Rock The Vote" );
	pVote.SetDescription( "Vote for the next map" );
	pVote.SetEndMessage( "The vote is over!" );
	
	// Create vote
	pVote.CallVote();
	bVoteCreated = true;
}

/**
Exclude a map from voting
**/
void ExcludeFromVoting( array<string> &inout MapCycle, string &in mapName )
{
	int searchIndex = MapCycle.find( mapName );
	if ( searchIndex >= 0 )
		MapCycle.removeAt( searchIndex );
}

/**
Get the maps that will be voted on
**/
string GetAvailableMapVote( array<string> &inout MapCycle )
{
	string Output;
	uint target;
	
	// Check our nominated maps
	if ( g_NominationList.length() > 0 )
	{
		target = Math::RandomInt(0, g_NominationList.length() - 1);
		Output = g_NominationList[ target ];
		g_NominationList.removeAt( target );
		
		// Remove this map from the MapCycle, so we don't get the option twice.
		int searchIndex = MapCycle.find( Output );
		if ( searchIndex >= 0 )
			MapCycle.removeAt( searchIndex );
	}
	// We found no nominated maps, grab from cycle
	else
	{
		target = Math::RandomInt(0, MapCycle.length() - 1);
		Output = MapCycle[ target ];
		MapCycle.removeAt( target );
	}
	
	return Output;
}

/**
Reset the Plugins mutable data
**/
void PluginReset()
{
	// Reset everything
	for ( uint x = 0; x < arrPlayerRTV.length(); x++ )
		arrPlayerRTV.removeAt( x );
	for ( uint x = 0; x < arrNominatedMap.length(); x++ )
		arrNominatedMap.removeAt( x );
	for ( uint x = 0; x < g_NominationList.length(); x++ )
		g_NominationList.removeAt( x );
	
	for ( int x = 0; x <= Globals.GetMaxClients(); x++ )
	{
		arrPlayerRTV.insertLast( false );
		arrNominatedMap.insertLast( "" );
	}
	
	bVoteCreated = false;
	bVoteOngoing = false;
	bVoteCountDown = false;
	flRTVTimer = Globals.GetCurrentTime() + 60;
	flTryStartVote = -1;
	g_flRTVTimer = -1;
	m_iCountDown = 15;
	flWaitTimer = Globals.GetCurrentTime() + FileSystem::Json::GrabFloat( hJsonData, "config", "waittimer" );
}

void OnMapInit()
{
	PluginReset();
}
